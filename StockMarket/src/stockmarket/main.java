package stockmarket;
public class main {
    public static void main(String[] args) {
        StockExchange StockPVH = new StockExchange();
        //Creating Observers to my stock
        DollarStockHolder obs1 = new DollarStockHolder(StockPVH);
        DollarStockHolder obs2 = new DollarStockHolder(StockPVH);
        GoldStockHolder obs3 = new GoldStockHolder(StockPVH);
        GoldStockHolder obs4 = new GoldStockHolder(StockPVH);
        
        obs1.setValueBuy(3.50);
        obs1.setValueSell(4.00);
        
        obs2.setValueSell(3.20);
        obs2.setValueSell(3.80);
        
        //Changing Subject States
        StockPVH.setPrices(4.10, 100);
        
        StockPVH.setPrices(3.80, 115);
        
        StockPVH.setPrices(3.50, 135);
        
    }
}
