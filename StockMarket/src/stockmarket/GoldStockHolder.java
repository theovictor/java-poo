package stockmarket;
public class GoldStockHolder implements Observer{
    private double GoldPrice;
    //Static variable used as a counter
    private static int obsIDTracker = 0;
    //Observer id
    private int obsID;
    //Will hold reference to the Market
    private Subject market;

    public GoldStockHolder(Subject market) {
        this.market = market;
        //Assign an Observer ID and increment static counter
        this.obsID = ++obsIDTracker;
        //Message of new Observer
        System.out.println("New Gold Observer ID: "+this.obsID);
        //Adding observer to subject arraylist
        this.market.attach(this);
    }
    @Override
    public void update(double Dollar, double Gold) {
        this.GoldPrice = Gold;
        BuyAndSell();
    }
    private void BuyAndSell() {
        System.out.println("Gold Observer ID: "+this.obsID+" Gold Price: "+this.GoldPrice);
        if(this.GoldPrice > 130){
            System.out.println("Selling Dollars!\n");
        }else if(this.GoldPrice <= 100){
            System.out.println("Buy Dollars!\n");
        }else{
            System.out.println("Holding Position!\n");
        }
    }
}